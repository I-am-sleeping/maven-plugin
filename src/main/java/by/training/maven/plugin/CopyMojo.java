package by.training.maven.plugin;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "copy")
public class CopyMojo extends AbstractMojo {

    @Parameter(property = "inputFile")
    private File inputFile;
    @Parameter(property = "outputFile")
    private File outputFile;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (inputFile.exists()) {
            try {
                Files.copy(inputFile.toPath(), outputFile.toPath(),
                        StandardCopyOption.REPLACE_EXISTING);
                getLog().info("Copy " + inputFile + " to " + outputFile);
            } catch (IOException e) {
                getLog().error("Cannot copy " + inputFile + " to " + outputFile,
                        e);
            }
        } else {
            getLog().error("File " + inputFile + " does not exist.");
        }
    }
}
